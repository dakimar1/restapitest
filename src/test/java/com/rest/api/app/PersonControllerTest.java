package com.rest.api.app;

import com.rest.api.app.controller.PersonController;
import com.rest.api.app.model.Person;
import com.rest.api.app.repository.PersonRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PersonController.class)
class PersonControllerTest extends AbstractTest {

    @MockBean
    private PersonRepository personRepository;


    @Autowired
    private MockMvc mockMvc;

    @Test
     void testGetAllPerson() throws Exception {
        when(personRepository.findAll()).thenReturn(returnListPersonsWithOne());
        this.mockMvc.perform(
                get("/api/persons"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].id").exists());
    }


    @Test
     void testGetNamePersonsByPostCodeSorted() throws Exception {
        when(personRepository.findByPostCode("0001")).thenReturn(returnListPersons());
        this.mockMvc.perform(
                        get("/api/personsname/0001/sorted"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").value("AAAAA"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").value("AAZZA"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2]").value("BBBBB"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[3]").value("CCCCC"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[4]").value("DDDDD"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[5]").value("EEEEE"));
    }




    @Test
     void testGetPerson() throws Exception {
        when(personRepository.findById((long) 0))
                .thenReturn(Optional.of(returnPerson()));
        this.mockMvc.perform(
                get("/api/person/0"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }




    @Test
    void testGetPersonFail() throws Exception {
        when(personRepository.findById((long) 0))
                .thenReturn(Optional.empty());
        this.mockMvc.perform(
                get("/api/persons/0"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }


    @Test
    void testGetPersonBadRequest() throws Exception {
        when(personRepository.findById((long) 0))
                .thenReturn(Optional.empty());
        this.mockMvc.perform(
                get("/api/persons/fff"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    void testCreatePerson() throws Exception {
        when(personRepository.save(any(Person.class)))
                .thenReturn(returnPerson());

        this.mockMvc.perform( MockMvcRequestBuilders
                .post("/api/person")
                .content(asJsonString(returnPerson()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

}

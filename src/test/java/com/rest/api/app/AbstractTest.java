package com.rest.api.app;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.rest.api.app.model.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AbstractTest {

    protected static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected List<Person> returnListPersonsWithOne() {
        List<Person> personList = new ArrayList<>();
        Person p1 = new Person(0, "First", "4899");
        personList.add(p1);
        return personList;
    }


    protected Optional<List<Person>> returnListPersons() {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person(0, "DDDDD", "0001"));
        personList.add(new Person(1, "BBBBB", "0001"));
        personList.add(new Person(2, "CCCCC", "0001"));
        personList.add(new Person(3, "AAAAA", "0001"));
        personList.add(new Person(4, "AAZZA", "0001"));
        personList.add(new Person(5, "EEEEE", "0001"));
        return Optional.of(personList);
    }

    protected Person returnPerson() {
        Person p1 = new Person(0, "First", "4899");
        return p1;
    }



}

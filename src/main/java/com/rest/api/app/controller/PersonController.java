package com.rest.api.app.controller;

import com.rest.api.app.exception.ResourceNotFoundException;
import com.rest.api.app.model.Person;
import com.rest.api.app.repository.PersonRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@Tag(name = "Person", description = "The Person API with documentation annotations")
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    /**
     * Get all persons list.
     *
     * @return the list
     */
    @Operation(summary = "Get all persons")
    @GetMapping("/persons")
    public List<Person> getAllPersons() {
        return personRepository.findAll();
    }

    /**
     * Get a person by id.
     *
     * @param personId the person id
     * @return the person by id
     * @throws ResourceNotFoundException the resource not found exception
     */
    @Operation(summary = "Get a person by id")
    @GetMapping("/person/{id}")
    public ResponseEntity<Person> getPersonById(
            @Validated @Min(0) @Parameter(description = "id of the person to return") @PathVariable(value = "id") Long personId)
            throws ResourceNotFoundException {
        Person person =
                personRepository
                        .findById(personId)
                        .orElseThrow(() -> new ResourceNotFoundException("Person not found on :: " + personId));
        return ResponseEntity.ok().body(person);
    }

    /**
     * Gets persons by Post code.
     *
     * @param postCode postCode
     * @return the persons by postCode
     * @throws ResourceNotFoundException the resource not found exception
     */
    @Operation(summary = "Get persons by post code")
    @GetMapping("/persons/{postCode}")
    public ResponseEntity<List<Person>> getPersonsByPostCode(
            @Validated @NotBlank @Parameter(description = "id of the person to return") @PathVariable(value = "postCode") String postCode)
            throws ResourceNotFoundException {
        List<Person> persons =
                personRepository
                        .findByPostCode(postCode)
                        .orElseThrow(() -> new ResourceNotFoundException("No person found with the post code :: " + postCode));
        return ResponseEntity.ok().body(persons);
    }

    /**
     * Gets persons by Post code sorted alphabetically
     *
     * @param postCode postCode
     * @return the persons by postCode sorted alphabetically
     * @throws ResourceNotFoundException the resource not found exception
     */
    @Operation(summary = "Get persons by post code")
    @GetMapping("/persons/sorted/{postCode}")
    public ResponseEntity<List<Person>> getPersonsByPostCodeSorted(
            @Validated @NotBlank @Parameter(description = "id of the person to return") @PathVariable(value = "postCode") String postCode)
            throws ResourceNotFoundException {

        List<Person> listPersons = personRepository
                .findByPostCode(postCode)
                .orElseThrow(() -> new ResourceNotFoundException("No person found with the post code :: " + postCode));

        List<Person> sortedListPerson = listPersons.stream()
                .sorted(Comparator.comparing(Person::getPostCode))
                .collect(Collectors.toList());

        return ResponseEntity.ok().body(sortedListPerson);
    }


    /**
     * Gets name persons by Post code sorted alphabetically
     *
     * @param postCode postCode
     * @return the name persons by postCode sorted alphabetically
     * @throws ResourceNotFoundException the resource not found exception
     */
    @Operation(summary = "Get name persons by post code sorted alphabetically")
    @GetMapping("/personsname/{postCode}/sorted")
    public ResponseEntity<List<String>> getNamePersonsByPostCodeSorted(
            @Validated @NotBlank @Parameter(description = "id of the person to return") @PathVariable(value = "postCode") String postCode)
            throws ResourceNotFoundException {

        List<Person> listPersons = personRepository
                .findByPostCode(postCode)
                .orElseThrow(() -> new ResourceNotFoundException("No person found with the post code :: " + postCode));

        List<String> sortedListNamePerson = listPersons.stream()
                .map(Person::getName)
                .sorted()
                .collect(Collectors.toList());

        return ResponseEntity.ok().body(sortedListNamePerson);
    }


    /**
     * Create person person.
     *
     * @param person the person
     * @return the person
     */
    @PostMapping("/person")
    @Operation(summary = "create a person")
    public Person createPerson(@Valid @RequestBody Person person) {
        return personRepository.save(person);
    }


/*
    /**
     * Create person from a list of person.
     *
     * @param persons list of person
     * @return List of the person
     */
    @PostMapping("/persons")
    @Operation(summary = "create a persons")
    public List<Person> createPersons(@Valid @RequestBody List<Person> persons) {
        return persons.stream().map(p -> personRepository.save(p)).collect(Collectors.toList());
    }


    /**
     * Update person response entity.
     *
     * @param personId the person id
     * @param personDetails the person details
     * @return the response entity
     * @throws ResourceNotFoundException the resource not found exception
     */
    @PutMapping("/persons/{id}")
    @Operation(summary = "update information of a person")
    public ResponseEntity<Person> updatePerson(
            @Valid @PathVariable(value = "id") Long personId, @Valid @RequestBody Person personDetails)
            throws ResourceNotFoundException {

        Person person =
                personRepository
                        .findById(personId)
                        .orElseThrow(() -> new ResourceNotFoundException("Person not found on :: " + personId));

        final Person updatedPerson = personRepository.save(person);
        return ResponseEntity.ok(updatedPerson);
    }

    /**
     * Delete person map.
     *
     * @param personId the person id
     * @return the map
     * @throws Exception the exception
     */
    @DeleteMapping("/person/{id}")
    @Operation(summary = "delete a person")
    public Map<String, Boolean> deletePerson(@Valid @PathVariable(value = "id") Long personId) throws Exception {
        Person person =
                personRepository
                        .findById(personId)
                        .orElseThrow(() -> new ResourceNotFoundException("Person not found on :: " + personId));

        personRepository.delete(person);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}

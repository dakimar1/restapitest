package com.rest.api.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The type Error Parameter exception..
 *
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ErrorParameterException extends Exception {

    /**
     * Instantiates a new Error Parameter exception.
     *
     * @param message the message
     */
    public ErrorParameterException(String message) {
        super(message);
    }
}

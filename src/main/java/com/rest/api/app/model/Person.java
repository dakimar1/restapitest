package com.rest.api.app.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "persons")
@EntityListeners(AuditingEntityListener.class)
public class Person {

    @Id
    private long id;

    @NotBlank
    @Column(name = "name", nullable = false)
    private String name;

    @NotBlank
    @Column(name = "post_code", nullable = false)
    private String postCode;

    public Person() {
    }

    public Person(long id, String name, String postCode) {
        this.id = id;
        this.name = name;
        this.postCode = postCode;
    }

    public Person(String name, String postCode) {
        this.name = name;
        this.postCode = postCode;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets postCode.
     *
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Sets postCode.
     *
     * @param postCode the post Code
     */
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }


}



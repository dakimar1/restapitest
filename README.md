# springboot-sample-app

Rest API sample app.

## Requirements

For building and running the application you need:

- [JDK 11](http://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.rest.api.app.RestApiApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

## Documentation 

You can find the documentation of the API in :

- local : localhost:8080/swagger-ui.html
- online ( cloud ) : https://restapipowerledger.herokuapp.com/swagger-ui.html (is a free serve so it shut down after 15 min of no use)
